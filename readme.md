
## What even is this?

This is a repo for my two Patreon projects. They share much of the code, so they're both here.

## How do I use this?

You need to get the Patreon API PHP library.

```
cd /path/to/private
composer require patreon/patreon
```